#!/bin/bash


set_led () {
	name=$1
	state=$2
	echo "$state" > "/sys/class/leds/$name/brightness"
}
toggle_leds () {
	case $1 in
		0)
			for i in {1..5}
			do
				set_led "a${i}_green" 0
				set_led "a${i}_red" 0
			done
			;;
		1)
			for i in {1..5}
			do
				set_led "a${i}_green" 1
			done
			;;
		2)
			for i in {1..5}
			do
				set_led "a${i}_green" 0
				set_led "a${i}_red" 1 
			done
			;;
		*)
			echo problem "$1"
			;;
	esac
}

file_test="/home/pi/revpi_emv.log"
output="*******************************************"
echo "$output"
echo "$output" >> "$file_test"

stty -F /dev/ttyRS485-0 "0:4:18b2:a30:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0"
stty -F /dev/ttyRS485-1 "0:4:18b2:a30:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0"

nr=0

piTest -w AOut,5000
while true; do
    mytime=$(date +"%T")
    flatS_AIn=$(piTest -1qr AIn)
    output="$mytime - $flatS_AIn"
    echo "$output"
    echo "$output" >> "$file_test"

    printf "%s\r\n" "$mytime" > /dev/ttyRS485-0
    printf "%s\r\n" "$mytime" > /dev/ttyRS485-1

    gpioset gpiochip0 28=$((nr % 2))

    toggle_leds $((nr % 3))
    nr=$((nr + 1))
    sleep 1
done


